package com.indonesia.ridwan.jsonarray;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    TextView textView;
    Button btn;
    Context context;
    String z  = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        textView = (TextView) findViewById(R.id.tx);
        btn = (Button) findViewById(R.id.btn);

        //getJsonArray();
        btn.setOnClickListener(this);
    }



    public void getJsonArray(){

        String url = "http://192.168.1.7/RegisterAndroid/get.php";

        StringRequest SR = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("data",response);

                try{
                    JSONObject object = new JSONObject(response);
                    JSONArray jsonArray = object.getJSONArray("data");
                    for(int i=0;i<jsonArray.length();i++){
                        JSONObject a = jsonArray.getJSONObject(i);
                        String email = a.getString("email");

                        z +=email;
                        textView.setText(z);

                    }
                }catch (JSONException e){
                    e.printStackTrace();
                }
            }
        },new Response.ErrorListener(){
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("data",error.toString());
            }
        });
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(SR);
    }

    @Override
    public void onClick(View view) {
        if (view == btn){


            try{
                getJsonArray();
            }catch (Exception e){
                Toast.makeText(getApplicationContext(),"Error Connection",Toast.LENGTH_LONG).show();
            }
        }
    }
}
